<?php
include('header.php');
if (empty($_SESSION['id'])) {
    die("
    
    <div class='errordiv'><h1>Please <a href='login.php'>Login</a>  To Continue</h1></div>");
}

?>


<div class="inventory-container">
    <h1>ORDERS</h1>
    <div><a href="ExportPDForders.php" class="dl-btn">Download PDF</a>
        <a href="ExportExcelorders.php" class="dl-btn2">Download Excel</a>
    </div>
    <div class="table-container">
        <table>

            <tr class="table-top-row">
                <td>Orderid.</td>
                <td>UserID</td>
                <td>UserName</td>
                <td>Products</td>
                <td>Total (Rs)</td>
                <td>Address</td>

            </tr>
            <?php
            $sql = "SELECT * FROM orders";
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
                while ($row = $result->fetch_assoc()) {
                    echo "
                        <tr>
                        <td>" . $row['id'] . "</td>
                        <td>" . $row['userid'] . "</td>
                        <td>" . $row['username'] . "</td>
                        <td>" . $row['products'] . "</td>
                        <td>" . $row['total'] . "</td>
                        <td>" . $row['address'] . "</td>
                  
                    </tr>
          ";
                }
            }
            ?>
        </table>
    </div>
</div>
<?php include('footer.php'); ?>