<?php

include('header.php');
?>


<div class="login-items-container">
  <div class="login-text-left">
    <h1>WELCOME BACK!</h1>
    <p class="login-text-left-p">Fashion Accessories to fit your needs</p>
  </div>
  <div class="login-form-right">
    <div class="login-form">
      <form action="login.php" id="loginFormCustom" method="POST">
        <h1 class="login-heading">LOGIN</h1>
        <input type="email" id="emailInput" class="custom-form-input-urbanmen" placeholder="Email" name="email" />

        <input type="password" class="custom-form-input-urbanmen" placeholder="Password" name="password" />
        <p class="password-conditions">Minimum 8 characters</p>
        <button type="submit" class="submit-btn-ln">Log In</button>
      </form>
    </div>
  </div>
</div>
<?php
if (!empty($_POST['email']) &&  !empty($_POST['password'])) {
  $email = $_POST['email'];
  $password = md5($_POST['password']);

  $sql = "SELECT * FROM user where email='$email' and password='$password' limit 1";

  $result = $conn->query($sql);

  if ($result->num_rows == 1) {
    $data = $result->fetch_assoc();
    $_SESSION['email'] = $data['email'];
    $_SESSION['id'] = $data['id'];
    $_SESSION['name'] = $data['name'];
    $_SESSION['username'] = $data['username'];

    header("Location:cart.php");
  } else {
    echo "Error: " . $sql . "<br>" . $conn->error;
  }
}
?>
<?php include('footer.php'); ?>