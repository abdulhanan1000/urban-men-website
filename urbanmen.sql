-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 01, 2021 at 10:49 AM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 8.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `urbanmen`
--

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` int(255) NOT NULL,
  `productid` int(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` double NOT NULL,
  `image` varchar(255) NOT NULL,
  `artno` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `inventory`
--

CREATE TABLE `inventory` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `category` varchar(50) NOT NULL,
  `price` double NOT NULL,
  `qty` int(11) NOT NULL,
  `artno` varchar(50) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `inventory`
--

INSERT INTO `inventory` (`id`, `name`, `category`, `price`, `qty`, `artno`, `image`) VALUES
(11, 'Guccii', 'Necklace', 18000, 5, 'NLC-123', 'NLC1.jpg'),
(13, 'Casio', 'Watch', 2000, 15, 'WTC-150', 'WTC1.jpg'),
(14, 'Caravel', 'Watch', 8000, 5, 'WTC-134', 'WTC2.jpg'),
(15, 'Rolex', 'Watch', 50000, 6, 'WTC-151', 'WTC3.jpg'),
(16, 'RADO', 'Watch', 80000, 10, 'WTC-120', 'WTC4.jpg'),
(17, 'Tesoro', 'Necklace', 3500, 10, 'NLC-350', 'NLC2.jpg'),
(18, 'L-V', 'Wallet', 3600, 7, 'WLT-521', 'WLT1.jpg'),
(19, 'Balisi', 'Wallet', 4500, 100, 'WLT-451', 'WLT2.jpg'),
(20, 'Tommy H.', 'Wallet', 7800, 12, 'WLT-421', 'WLT3.jpg'),
(21, 'Dubai Zawrat', 'Necklace', 4500, 20, 'NLC-450', 'NLC3.jpg'),
(22, 'Uxama G n J', 'Braclet', 8000, 21, 'BRT-810', 'BRT1.jpg'),
(23, 'HUB', 'Wallet', 2300, 21, 'WLT-412', 'WLT4.jpg'),
(24, 'Rolay Tag', 'Wallet', 3500, 24, 'WLT-021', 'WLT5.jpg'),
(25, 'Mont Blanc', 'Wallet', 6500, 13, 'WLT-111', 'WLT6.jpg'),
(26, 'ARY', 'Necklace', 120000, 2, 'NLC-480', 'NLC4.jpg'),
(27, 'Solitaire', 'Necklace', 45000, 4, 'NLC-380', 'NLC5.jpg'),
(28, 'Jewelex', 'Necklace', 189000, 5, 'NLC-520', 'NLC6.jpg'),
(29, 'Levis', 'Belt', 3400, 32, 'BLT-174', 'BLT1.jpg'),
(30, 'Versace', 'Belt', 10000, 16, 'BLT-164', 'BLT2.jpg'),
(31, 'DG Belt', 'Belt', 6000, 15, 'BLT-114', 'BLT3.jpg'),
(32, 'Bangles', 'Braclet', 15000, 12, 'BRT-710', 'BRT2.jpg'),
(33, 'Silver925', 'Braclet', 18000, 19, 'BRT-180', 'BRT3.jpg'),
(34, 'Hanif Jewellers', 'Braclet', 16000, 20, 'BRT-310', 'BRT4.jpg'),
(43, 'Digital Watch', 'Watch', 1200, 5, 'WTC-600', 'gsmarena_002.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(255) NOT NULL,
  `userid` int(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `products` varchar(255) NOT NULL,
  `total` double NOT NULL,
  `address` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `userid`, `username`, `products`, `total`, `address`) VALUES
(1, 26, ' jack ', ' BRT-710, BRT-180,  ', 33000, 'gfhsdhdrfh'),
(2, 26, ' jack ', ' NLC-350, NLC-450, WTC-134,  ', 16000, 'house 60');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `username`, `email`, `password`) VALUES
(19, 'hanan', 'hanan10', 'hanan@gmail.com', 'e6044cc09720960333854db57f3029d7'),
(20, 'Ali', 'Ali10', 'ali@gmail.com', '984d8144fa08bfc637d2825463e184fa'),
(21, 'ahmed', 'ahmed10', 'ahmed10@gmail.com', '32aa2fd87338e241978c48ab319641bc'),
(22, 'jack', 'jack10', 'jack10@gmail.com', '25d55ad283aa400af464c76d713c07ad'),
(23, 'Ahmed', 'Ahmed', 'ahmed10@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b'),
(25, 'Usama', 'Usama10', 'usama@gmail.com', 'b050fb046cbe6091b0fdf824028a989a'),
(26, 'jack', 'Jack1234', 'jack@gmail.com', 'b65185c87b930a4406ed47439fea139c');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inventory`
--
ALTER TABLE `inventory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `inventory`
--
ALTER TABLE `inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
