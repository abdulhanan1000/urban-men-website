<?php
session_start();
ob_start();
include('db.php')
?>
<!DOCTYPE html>
<html>
<!-- ********* © URBANMEN **********
      THIS WEBSITE IS MADE BY:
       1. ABDUL HANAN JAVAID (2580)
       2. MUHAMMAD USAMA (2267)
       3. MUHAMMAD SAAD ZAFAR (2132)
       -->

<head>
    <title>UrbanMen</title>
    <link rel="preconnect" href="https://fonts.gstatic.com" />
    <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" />
    <link rel="stylesheet" href="css/styles.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
</head>

<body>
    <nav class="custom-nav-urbanmen">
        <div class="nav-container">
            <a href="index.php" class="urbanmen-logo">URBANMEN</a>
            <div class="navlinks-container">
                <ul>
                    <li><a href="watches.php" class="watchlink">Watches</a></li>
                    <li><a href="wallets.php" class="walletslink">Wallets</a></li>
                    <li>
                        <a href="bracelets.php" class="braceletslink">Bracelets</a>
                    </li>
                    <li>
                        <a href="necklaces.php" class="necklaceslink">Necklaces</a>
                    </li>
                    <li><a href="belts.php" class="beltslink">Belts</a></li>
                </ul>
            </div>
            <div class="navlinks-left-container">
                <a href="cart.php" class="cart-logo"><i class="fas fa-shopping-cart"></i></a>
                <?php if (empty($_SESSION['id'])) {

                ?>
                    <a href="register.php" class="register-btn">Register</a>
                    <a href="login.php" class="login-btn">Login</a>

                <?php
                } else {
                    echo "<h2 style='color:white; display:inline-block'>" . $_SESSION['name'] . "</h2>";
                    echo "<a class='logout-btn' href='logout.php'>Logout</a>";
                }
                ?>
            </div>
        </div>
    </nav>
    <div class="watch-sub-menu nav-submenu">
        <div class="submenu-items-container">
            <div class="item-1">
                <img class="image-fit-products" src="https://via.placeholder.com/420x200" alt="" />
                <p class="custom-item-heading">Lorem, ipsum.</p>
                <p class="custom-item-text">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum
                    doloremque voluptas rem sapiente fuga iusto.
                </p>
            </div>
            <div class="item-2">
                <img class="image-fit-products" src="https://via.placeholder.com/420x200" alt="" />
                <p class="custom-item-heading">Lorem, ipsum.</p>
                <p class="custom-item-text">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum
                    doloremque voluptas rem sapiente fuga iusto.
                </p>
            </div>
            <div class="item-3">
                <img class="image-fit-products" src="https://via.placeholder.com/420x200" alt="" />
                <p class="custom-item-heading">Lorem, ipsum.</p>
                <p class="custom-item-text">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum
                    doloremque voluptas rem sapiente fuga iusto.
                </p>
            </div>
            <div class="item-4">
                <img class="image-fit-products" src="https://via.placeholder.com/420x200" alt="" />
                <p class="custom-item-heading">Lorem, ipsum.</p>
                <p class="custom-item-text">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum
                    doloremque voluptas rem sapiente fuga iusto.
                </p>
            </div>
        </div>
    </div>
    <div class="wallets-sub-menu nav-submenu">
        <div class="submenu-items-container">
            <div class="item-1">
                <img class="image-fit-products" src="https://via.placeholder.com/420x200" alt="" />
                <p class="custom-item-heading">Lorem, ipsum.</p>
                <p class="custom-item-text">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum
                    doloremque voluptas rem sapiente fuga iusto.
                </p>
            </div>
            <div class="item-2">
                <img class="image-fit-products" src="https://via.placeholder.com/420x200" alt="" />
                <p class="custom-item-heading">Lorem, ipsum.</p>
                <p class="custom-item-text">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum
                    doloremque voluptas rem sapiente fuga iusto.
                </p>
            </div>
            <div class="item-3">
                <img class="image-fit-products" src="https://via.placeholder.com/420x200" alt="" />
                <p class="custom-item-heading">Lorem, ipsum.</p>
                <p class="custom-item-text">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum
                    doloremque voluptas rem sapiente fuga iusto.
                </p>
            </div>
            <div class="item-4">
                <img class="image-fit-products" src="https://via.placeholder.com/420x200" alt="" />
                <p class="custom-item-heading">Lorem, ipsum.</p>
                <p class="custom-item-text">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum
                    doloremque voluptas rem sapiente fuga iusto.
                </p>
            </div>
        </div>
    </div>
    <!-- All these submenus will be display: none currently and display: flex after -->
    <div class="bracelets-sub-menu nav-submenu">
        <div class="submenu-items-container">
            <div class="item-1">
                <img class="image-fit-products" src="https://via.placeholder.com/420x200" alt="" />
                <p class="custom-item-heading">Lorem, ipsum.</p>
                <p class="custom-item-text">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum
                    doloremque voluptas rem sapiente fuga iusto.
                </p>
            </div>
            <div class="item-2">
                <img class="image-fit-products" src="https://via.placeholder.com/420x200" alt="" />
                <p class="custom-item-heading">Lorem, ipsum.</p>
                <p class="custom-item-text">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum
                    doloremque voluptas rem sapiente fuga iusto.
                </p>
            </div>
            <div class="item-3">
                <img class="image-fit-products" src="https://via.placeholder.com/420x200" alt="" />
                <p class="custom-item-heading">Lorem, ipsum.</p>
                <p class="custom-item-text">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum
                    doloremque voluptas rem sapiente fuga iusto.
                </p>
            </div>
            <div class="item-4">
                <img class="image-fit-products" src="https://via.placeholder.com/420x200" alt="" />
                <p class="custom-item-heading">Lorem, ipsum.</p>
                <p class="custom-item-text">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum
                    doloremque voluptas rem sapiente fuga iusto.
                </p>
            </div>
        </div>
    </div>
    <div class="necklaces-sub-menu nav-submenu">
        <div class="submenu-items-container">
            <div class="item-1">
                <img class="image-fit-products" src="https://via.placeholder.com/420x200" alt="" />
                <p class="custom-item-heading">Lorem, ipsum.</p>
                <p class="custom-item-text">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum
                    doloremque voluptas rem sapiente fuga iusto.
                </p>
            </div>
            <div class="item-2">
                <img class="image-fit-products" src="https://via.placeholder.com/420x200" alt="" />
                <p class="custom-item-heading">Lorem, ipsum.</p>
                <p class="custom-item-text">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum
                    doloremque voluptas rem sapiente fuga iusto.
                </p>
            </div>
            <div class="item-3">
                <img class="image-fit-products" src="https://via.placeholder.com/420x200" alt="" />
                <p class="custom-item-heading">Lorem, ipsum.</p>
                <p class="custom-item-text">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum
                    doloremque voluptas rem sapiente fuga iusto.
                </p>
            </div>
            <div class="item-4">
                <img class="image-fit-products" src="https://via.placeholder.com/420x200" alt="" />
                <p class="custom-item-heading">Lorem, ipsum.</p>
                <p class="custom-item-text">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum
                    doloremque voluptas rem sapiente fuga iusto.
                </p>
            </div>
        </div>
    </div>
    <div class="belts-sub-menu nav-submenu">
        <div class="submenu-items-container">
            <div class="item-1">
                <img class="image-fit-products" src="https://via.placeholder.com/420x200" alt="" />
                <p class="custom-item-heading">Lorem, ipsum.</p>
                <p class="custom-item-text">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum
                    doloremque voluptas rem sapiente fuga iusto.
                </p>
            </div>
            <div class="item-2">
                <img class="image-fit-products" src="https://via.placeholder.com/420x200" alt="" />
                <p class="custom-item-heading">Lorem, ipsum.</p>
                <p class="custom-item-text">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum
                    doloremque voluptas rem sapiente fuga iusto.
                </p>
            </div>
            <div class="item-3">
                <img class="image-fit-products" src="https://via.placeholder.com/420x200" alt="" />
                <p class="custom-item-heading">Lorem, ipsum.</p>
                <p class="custom-item-text">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum
                    doloremque voluptas rem sapiente fuga iusto.
                </p>
            </div>
            <div class="item-4">
                <img class="image-fit-products" src="https://via.placeholder.com/420x200" alt="" />
                <p class="custom-item-heading">Lorem, ipsum.</p>
                <p class="custom-item-text">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum
                    doloremque voluptas rem sapiente fuga iusto.
                </p>
            </div>
        </div>
    </div>