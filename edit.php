<?php

include('header.php');
if (empty($_SESSION['id'])) {
    die("
    
    <div class='errordiv'><h1>Please <a href='login.php'>Login</a>  To Continue</h1></div>");
}


$sql = "SELECT * FROM inventory where `id`=" . $_GET['id'];

$result = $conn->query($sql);

if ($result->num_rows === 1) {
    $data = $result->fetch_assoc();
}

?>



<div class="addproducts-container">
    <div class="addproducts-sub-container">
        <form action="<?php echo "edit.php?id=" . $_GET['id'] ?>" method="POST">
            <div class="form-item-addproduct">
                <label for="" class="product-form-label">Name</label>
                <input type="text" name="name" id="" class="product-form-input" value="<?php echo $data['name'] ?>" />
            </div>
            <div class="form-item-addproduct">
                <label for="" class="product-form-label">Category</label>
                <select name="category" id="" class="product-form-input product-form-input-select" value="<?php echo $data['category'] ?>">
                    <?php
                    if ($data['category'] == "Watch") {
                        echo '<option value="Watch" selected>Watch</option>
                    <option value="Necklace">Necklace</option>
                    <option value="Belt">Belt</option>
                    <option value="Bracelet">Bracelet</option>
                    <option value="Wallet">Wallet</option>';
                    } elseif ($data['category'] == "Necklace") {
                        echo '<option value="Watch" >Watch</option>
                    <option value="Necklace" selected>Necklace</option>
                    <option value="Belt">Belt</option>
                    <option value="Bracelet">Bracelet</option>
                    <option value="Wallet">Wallet</option>';
                    } elseif ($data['category'] == "Belt") {
                        echo '<option value="Watch" >Watch</option>
                    <option value="Necklace">Necklace</option>
                    <option value="Belt" selected>Belt</option>
                    <option value="Bracelet">Bracelet</option>
                    <option value="Wallet">Wallet</option>';
                    } elseif ($data['category'] == "Bracelet") {
                        echo '<option value="Watch">Watch</option>
                    <option value="Necklace">Necklace</option>
                    <option value="Belt">Belt</option>
                    <option value="Bracelet" selected>Bracelet</option>
                    <option value="Wallet">Wallet</option>';
                    } elseif ($data['category'] == "Wallet") {
                        echo '<option value="Watch">Watch</option>
                    <option value="Necklace">Necklace</option>
                    <option value="Belt">Belt</option>
                    <option value="Bracelet">Bracelet</option>
                    <option value="Wallet" selected>Wallet</option>';
                    }

                    ?>

                </select>
            </div>
            <div class="form-item-addproduct">
                <label for="" class="product-form-label">Price</label>
                <input type="number" name="price" id="" class="product-form-input" value="<?php echo $data['price'] ?>" />
            </div>
            <div class="form-item-addproduct">
                <label for="" class="product-form-label">Quantity</label>
                <input type="number" name="qty" id="" class="product-form-input" value="<?php echo $data['qty'] ?>" />
            </div>
            <div class="form-item-addproduct">
                <label for="" class="product-form-label">Article Number</label>
                <input type="text" name="artno" id="" class="product-form-input" value="<?php echo $data['artno'] ?>" />
            </div>
            <div class="form-item-addproduct">
                <label for="" class="product-form-label">Upload Image</label>
                <input type="file" name="upimg" id="" class="product-file-input" />
            </div>
            <button type="submit" class="addpruduct-btn">Submit</button>
        </form>

        <?php
        if (!empty($_POST['name']) && !empty($_POST['category']) && !empty($_POST['price']) && !empty($_POST['qty']) && !empty($_POST['artno'])) {
            $name = $_POST['name'];
            $category = $_POST['category'];
            $price = $_POST['price'];
            $qty = $_POST['qty'];
            $artno = $_POST['artno'];

            $sql = "UPDATE inventory set `name` = '$name', `qty`=$qty, `category`='$category', `price`=$price, `artno`='$artno' WHERE `id`=" . $_GET['id'];
            if ($conn->query($sql) == TRUE) {
                echo "Data updated";
            } else {
                echo "ERROR:" . $sql . "<br>" . $conn->error;
            }
            $conn->close();
        }


        ?>
    </div>
</div>

<?php include('footer.php'); ?>