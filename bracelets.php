<?php
include('header.php'); ?>



<section class="product-showcase product-showcase-bracelets">
  <!-- This section will have background images in CSS -->
</section>
<section class="top-products-container">
  <div class="top-products">
    <p class="custom-heading-1">Bracelets</p>
    <div class="products-items-container">
      <?php
      $sql = "SELECT * FROM inventory";
      $result = $conn->query($sql);
      if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
          if ($row['category'] == 'Braclet') {
            echo "<div class='item-1-products item-products'>
                    <img class='image-fit-products' src='images/" . $row['image'] . "' alt='' />
                    <p class='custom-product-heading'>" . $row['name'] . "</p>
                    <p class='product-price'>Rs. " . $row['price'] . "</p>
                    <form class='form-cart'>
                      <input type='hidden' class='p-image' value=" . $row['image'] . ">
                      <input type='hidden' class='p-name' value=" . $row['name'] . ">
                      <input type='hidden' class='p-price' value=" . $row['price'] . ">
                      <input type='hidden' class='p-id' value=" . $row['id'] . ">
                      <input type='hidden' class='p-artno' value=" . $row['artno'] . ">
                    <a href='' class='addcartproduct-btn'>Add To Cart</a>
                    </form>
                  </div>";
          }
        }
      } ?>
      <script>
        $(document).on("click", ".addcartproduct-btn", function(e) {
          e.preventDefault();
          var form = $(this).closest(".form-cart");
          var image = form.find(".p-image").val();
          var name = form.find(".p-name").val();
          var price = form.find(".p-price").val();
          var id = form.find(".p-id").val();
          var artno = form.find(".p-artno").val();


          $.ajax({
            url: "addtocart.php",
            method: "post",
            data: {
              id: id,
              name: name,
              price: price,
              image: image,
              artno: artno
            },
            success: function(data) {
              alert("added to cart");
            }
          })
        })
      </script>
    </div>
  </div>

</section>
<?php include('footer.php'); ?>