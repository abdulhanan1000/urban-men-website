<?php
include('header.php');
?>

<div class="registration-content">
  <div class="register-text-left">
    <h1 class="welcome-registration-txt">WELCOME TO URBANMEN</h1>
    <p class="register-text-left-p">Redefining Men's Fashion.</p>
  </div>
  <div class="registration-form-right">
    <div class="registration-form">
      <p class="registration-heading">REGISTER</p>
      <form action="register.php" method="POST" id="registrationFormCustom">
        <input type="text" id="name" class="custom-form-input-urbanmen" placeholder="Name" name="name" />

        <input type="text" class="custom-form-input-urbanmen" placeholder="Username" name="username" />

        <input type="email" id="emailInput" class="custom-form-input-urbanmen" placeholder="Email" name="email" />

        <input type="password" pattern="^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\S+$).{8,}$" class="custom-form-input-urbanmen password-in" placeholder="Password" required name="password" />
        <p class="password-conditions">Minimum 8 characters, 1 Capital, 1 Small Letter and number</p>

        <button type="submit" class="submit-btn-rg">Sign Up</button>
      </form>
    </div>
  </div>
</div>
<?php
if (!empty($_POST['name'] && $_POST['username'] && $_POST['email'] && $_POST['password'])) {
  $name = $_POST['name'];
  $username = $_POST['username'];
  $email = $_POST['email'];
  $password = md5($_POST['password']);



  $sql = "INSERT INTO user (name, username, email, password) VALUES ('$name', '$username', '$email', '$password') ";

  if ($conn->query($sql) === TRUE) {
    header('Location:login.php');
  } else {
    echo "Error: " . $sql . "<br>" . $conn->error;
  }
  $conn->close();
}
?><?php include('footer.php'); ?>


<script>
  var myRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})");
  document.querySelector(".password-in").addEventListener("keyup", function() {
    if (myRegex.test(document.querySelector(".password-in").value)) {
      document.querySelector(".password-conditions").innerHTML = "Pasword Meets Required Format"

    } else {
      document.querySelector(".password-conditions").innerHTML = "Minimum 8 characters, 1 Capital, 1 Small Letter and number"

    }
  })
</script>