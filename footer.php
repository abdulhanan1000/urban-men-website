<footer class="urbanmen-footer">
    <div class="footer-container">
        <div class="urbanmen-logo-footer">
            <p>URBANMEN</p>
        </div>
        <div class="urbanmen-copyright">
            <p>© 2021 URBANMEN. All rights reserved.</p>
        </div>
        <div class="urbanmen-socials">
            <a href=""><i class="fab fa-facebook-square"></i></a>
            <a href=""><i class="fab fa-instagram"></i></a>
            <a href=""><i class="fab fa-twitter"></i></a>
            <a href=""><i class="fab fa-linkedin"></i></a>
        </div>
    </div>
</footer>
</body>

</html>
<?php
ob_end_flush();
?>