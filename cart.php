<?php
include('header.php'); ?>


<div class="cart-outer">
  <div class="cart-main-container">
    <?php
    if (!$_SESSION['name']) {
      echo "<h1>YOUR CART</h1>";
    } else {
      echo "<h1>" . $_SESSION['name'] . "'s" . " CART</h1>";
    }
    $sql = "SELECT * FROM cart";
    $result = $conn->query($sql);
    $total = 0;
    $products = "";
    if ($result->num_rows > 0) {
      while ($row = $result->fetch_assoc()) {
        $total += $row['price'];
        $products .= $row['artno'];
        $products .= ", ";
    ?>

        <div class="cart-content-holder">
          <div class="cart-item-container">
            <div class="img-container">
              <img class="image-fit-products" src="<?php echo "images/" . $row['image'] ?>" alt="" />
            </div>

            <div class="product-text">
              <p class="product-title"><?php echo $row['name'] ?></p>
              <p class="product-details"><?php echo $row['artno'] ?></p>

            </div>
            <div>
              <p class="product-price"><?php echo "Rs. " . $row['price'] ?></p>
            </div>
          </div>
        </div>

    <?php
      }
    }
    ?>
    <p class="product-title">Address:</p>

    <form class='form-order'>
      <input type='hidden' class='p-userid' value=" <?php echo $_SESSION['id'] ?> ">
      <input type='hidden' class='p-username' value=" <?php echo $_SESSION['name'] ?> ">
      <input type='hidden' class='p-products' value=" <?php echo $products ?> ">
      <input type='hidden' class='p-total' value="<?php echo $total ?> ">
      <input type='text' class='p-address' placeholder="Shipping Address Here">
    </form>
    <hr class="cart-hr" />
    <div class="cart-bill">
      <p class="total-amount">Total Amount</p>
      <p class="product-price"><?php echo "Rs. " . $total ?></p>
    </div>

    <a href="" class="checkout-btn">Place Order</a>
    <a href="clearcart.php" class="cleart-cart">Clear Cart</a>
  </div>
</div>
<script>
  $(document).on("click", ".checkout-btn", function(e) {
    e.preventDefault();
    if ($(".p-address").val() == "") {
      alert("Kindly enter an address");
    } else {

      var form = $(".form-order");
      var userid = form.find(".p-userid").val();
      var username = form.find(".p-username").val();
      var products = form.find(".p-products").val();
      var total = form.find(".p-total").val();
      var address = form.find(".p-address").val();


      $.ajax({
        url: "placeorder.php",
        method: "post",
        data: {
          userid: userid,
          username: username,
          products: products,
          total: total,
          address: address
        },
        success: function(data) {
          alert("Order Placed And Will Be Delivered To Your Address Soon!");
        }
      })
    }
  })
</script>
<?php include('footer.php'); ?>