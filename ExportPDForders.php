<?php
require 'vendor/autoload.php';

use Dompdf\Dompdf;

include("db.php");


$sql = "SELECT * FROM orders";
$result = $conn->query($sql);

$out = "<table border='0.5px' width='100%'>
            <tr>
             <td>Orderid.</td>
                <td>UserID</td>
                <td>UserName</td>
                <td>Products</td>
                <td>Total (Rs)</td>
                <td>Address</td>
            </tr>
    
    ";

if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $out .= "
        <tr>
      <td>" . $row['id'] . "</td>
                        <td>" . $row['userid'] . "</td>
                        <td>" . $row['username'] . "</td>
                        <td>" . $row['products'] . "</td>
                        <td>" . $row['total'] . "</td>
                        <td>" . $row['address'] . "</td>
        ";
    }
}

$out .= "</table>";

// instantiate and use the dompdf class
$dompdf = new Dompdf();
$dompdf->loadHtml($out);

// (Optional) Setup the paper size and orientation
$dompdf->setPaper('A4', 'landscape');

// Render the HTML as PDF
$dompdf->render();

// Output the generated PDF to Browser
$dompdf->stream();
