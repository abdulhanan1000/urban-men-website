<?php
include("db.php");

if (isset($_FILES['image'])) {
    $errors = array();
    $file_name = $_FILES['image']['name'];
    $file_size = $_FILES['image']['size'];
    $file_tmp = $_FILES['image']['tmp_name'];
    $file_type = $_FILES['image']['type'];
    $file_ext = strtolower(end(explode('.', $_FILES['image']['name'])));

    $extensions = array("jpeg", "jpg", "png");

    if (in_array($file_ext, $extensions) === false) {
      $errors[] = "extension not allowed, please choose a JPEG or PNG file.";
    }

    if ($file_size > 5097152) {
      $errors[] = 'File size must be below 5 MB';
    }

    if (empty($errors) == true) {
      move_uploaded_file($file_tmp, "images/" . $file_name);
      echo "Success";
    } else {
      print_r($errors);
    }
  }

if (!empty($_POST['name']) && !empty($_POST['category']) && !empty($_POST['price']) && !empty($_POST['qty']) && !empty($_POST['artno'])) {
    $name = $_POST['name'];
    $category = $_POST['category'];
    $price = $_POST['price'];
    $qty = $_POST['qty'];
    $artno = $_POST['artno'];

    $sql = "INSERT INTO inventory (name,category,price,qty,artno,image) VALUES ('$name', '$category',$price,$qty,'$artno','$file_name')";
    if ($conn->query($sql) == TRUE) {
      echo "Data added";
    } else {
      echo "ERROR:" . $sql . "<br>" . $conn->error;
    }
    $conn->close();
  }
?>