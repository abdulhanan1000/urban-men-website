<?php
include('header.php');
if (empty($_SESSION['id'])) {
  die("
    
    <div class='errordiv'><h1>Please <a href='login.php'>Login</a>  To Continue</h1></div>");
}

?>


<div class="inventory-container">
  <h1>INVENTORY</h1>
  <div><a href="ExportPDF.php" class="dl-btn">Download PDF</a>
    <a href="ExportExcel.php" class="dl-btn2">Download Excel</a>
  </div>

  <div class="table-container">
    <table>

      <tr class="table-top-row">
        <td>Name</td>
        <td>Article Number</td>
        <td>Category</td>
        <td>Quantity</td>
        <td>Price</td>
        <td>Edit</td>
        <td>Delete</td>
      </tr>
      <?php
      $sql = "SELECT * FROM inventory";
      $result = $conn->query($sql);
      if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
          echo "
          <tr>
        <td>" . $row['name'] . "</td>
        <td>" . $row['artno'] . "</td>
        <td>" . $row['category'] . "</td>
        <td>" . $row['qty'] . "</td>
        <td>" . $row['price'] . "</td>
       <td><a href='edit.php?id=" . $row['id'] . "' class='editbtn'>Edit</a></td>
        <td> <a href='delete.php?id=" . $row['id'] . "' class='delbtn'>Delete</a></td>
      </tr>
          ";
        }
      }
      ?>

      <!-- OLD PLACEHOLDER DATA -->
      <!-- <tr>
        <td>Rolex</td>
        <td>W-121</td>
        <td>Watch</td>
        <td>3</td>
        <td>Rs. 180,000</td>
      </tr>
      <tr>
        <td>Tommy Hilfiger</td>
        <td>WT-112</td>
        <td>Wallet</td>
        <td>8</td>
        <td>Rs. 8000</td>
      </tr>
      <tr>
        <td>Urban Outfitters</td>
        <td>BR-667</td>
        <td>Bracelet</td>
        <td>18</td>
        <td>Rs. 1000</td>
      </tr>
      <tr>
        <td>Silver - 925</td>
        <td>N-912</td>
        <td>Necklace</td>
        <td>3</td>
        <td>Rs. 3,000</td>
      </tr>
      <tr>
        <td>Allah Buksh</td>
        <td>BE-434</td>
        <td>Belt</td>
        <td>3</td>
        <td>Rs. 999</td>
      </tr>
      <tr>
        <td>Silver - 925</td>
        <td>N-912</td>
        <td>Necklace</td>
        <td>3</td>
        <td>Rs. 3,000</td>
      </tr>
      <tr>
        <td>Rolex</td>
        <td>W-121</td>
        <td>Watch</td>
        <td>3</td>
        <td>Rs. 180,000</td>
      </tr>
      <tr>
        <td>Tommy Hilfiger</td>
        <td>WT-112</td>
        <td>Wallet</td>
        <td>8</td>
        <td>Rs. 8000</td>
      </tr>
      <tr>
        <td>Urban Outfitters</td>
        <td>BR-667</td>
        <td>Bracelet</td>
        <td>18</td>
        <td>Rs. 1000</td>
      </tr>
      <tr>
        <td>Silver - 925</td>
        <td>N-912</td>
        <td>Necklace</td>
        <td>3</td>
        <td>Rs. 3,000</td>
      </tr>
      <tr>
        <td>Allah Buksh</td>
        <td>BE-434</td>
        <td>Belt</td>
        <td>3</td>
        <td>Rs. 999</td>
      </tr>
      <tr>
        <td>Silver - 925</td>
        <td>N-912</td>
        <td>Necklace</td>
        <td>3</td>
        <td>Rs. 3,000</td>
      </tr>
      <tr>
        <td>Rolex</td>
        <td>W-121</td>
        <td>Watch</td>
        <td>3</td>
        <td>Rs. 180,000</td>
      </tr>
      <tr>
        <td>Tommy Hilfiger</td>
        <td>WT-112</td>
        <td>Wallet</td>
        <td>8</td>
        <td>Rs. 8000</td>
      </tr>
      <tr>
        <td>Urban Outfitters</td>
        <td>BR-667</td>
        <td>Bracelet</td>
        <td>18</td>
        <td>Rs. 1000</td>
      </tr>
      <tr>
        <td>Silver - 925</td>
        <td>N-912</td>
        <td>Necklace</td>
        <td>3</td>
        <td>Rs. 3,000</td>
      </tr>
      <tr>
        <td>Allah Buksh</td>
        <td>BE-434</td>
        <td>Belt</td>
        <td>3</td>
        <td>Rs. 999</td>
      </tr>
      <tr>
        <td>Silver - 925</td>
        <td>N-912</td>
        <td>Necklace</td>
        <td>3</td>
        <td>Rs. 3,000</td>
      </tr>
      <tr>
        <td>Rolex</td>
        <td>W-121</td>
        <td>Watch</td>
        <td>3</td>
        <td>Rs. 180,000</td>
      </tr>
      <tr>
        <td>Tommy Hilfiger</td>
        <td>WT-112</td>
        <td>Wallet</td>
        <td>8</td>
        <td>Rs. 8000</td>
      </tr>
      <tr>
        <td>Urban Outfitters</td>
        <td>BR-667</td>
        <td>Bracelet</td>
        <td>18</td>
        <td>Rs. 1000</td>
      </tr>
      <tr>
        <td>Silver - 925</td>
        <td>N-912</td>
        <td>Necklace</td>
        <td>3</td>
        <td>Rs. 3,000</td>
      </tr>
      <tr>
        <td>Allah Buksh</td>
        <td>BE-434</td>
        <td>Belt</td>
        <td>3</td>
        <td>Rs. 999</td>
      </tr>
      <tr>
        <td>Silver - 925</td>
        <td>N-912</td>
        <td>Necklace</td>
        <td>3</td>
        <td>Rs. 3,000</td>
      </tr>
      <tr>
        <td>Rolex</td>
        <td>W-121</td>
        <td>Watch</td>
        <td>3</td>
        <td>Rs. 180,000</td>
      </tr>
      <tr>
        <td>Tommy Hilfiger</td>
        <td>WT-112</td>
        <td>Wallet</td>
        <td>8</td>
        <td>Rs. 8000</td>
      </tr>
      <tr>
        <td>Urban Outfitters</td>
        <td>BR-667</td>
        <td>Bracelet</td>
        <td>18</td>
        <td>Rs. 1000</td>
      </tr>
      <tr>
        <td>Silver - 925</td>
        <td>N-912</td>
        <td>Necklace</td>
        <td>3</td>
        <td>Rs. 3,000</td>
      </tr>
      <tr>
        <td>Allah Buksh</td>
        <td>BE-434</td>
        <td>Belt</td>
        <td>3</td>
        <td>Rs. 999</td>
      </tr>
      <tr>
        <td>Silver - 925</td>
        <td>N-912</td>
        <td>Necklace</td>
        <td>3</td>
        <td>Rs. 3,000</td>
      </tr>
      <tr>
        <td>Rolex</td>
        <td>W-121</td>
        <td>Watch</td>
        <td>3</td>
        <td>Rs. 180,000</td>
      </tr>
      <tr>
        <td>Tommy Hilfiger</td>
        <td>WT-112</td>
        <td>Wallet</td>
        <td>8</td>
        <td>Rs. 8000</td>
      </tr>
      <tr>
        <td>Urban Outfitters</td>
        <td>BR-667</td>
        <td>Bracelet</td>
        <td>18</td>
        <td>Rs. 1000</td>
      </tr>
      <tr>
        <td>Silver - 925</td>
        <td>N-912</td>
        <td>Necklace</td>
        <td>3</td>
        <td>Rs. 3,000</td>
      </tr>
      <tr>
        <td>Allah Buksh</td>
        <td>BE-434</td>
        <td>Belt</td>
        <td>3</td>
        <td>Rs. 999</td>
      </tr>
      <tr>
        <td>Silver - 925</td>
        <td>N-912</td>
        <td>Necklace</td>
        <td>3</td>
        <td>Rs. 3,000</td>
      </tr>
      <tr>
        <td>Rolex</td>
        <td>W-121</td>
        <td>Watch</td>
        <td>3</td>
        <td>Rs. 180,000</td>
      </tr>
      <tr>
        <td>Tommy Hilfiger</td>
        <td>WT-112</td>
        <td>Wallet</td>
        <td>8</td>
        <td>Rs. 8000</td>
      </tr>
      <tr>
        <td>Urban Outfitters</td>
        <td>BR-667</td>
        <td>Bracelet</td>
        <td>18</td>
        <td>Rs. 1000</td>
      </tr>
      <tr>
        <td>Silver - 925</td>
        <td>N-912</td>
        <td>Necklace</td>
        <td>3</td>
        <td>Rs. 3,000</td>
      </tr>
      <tr>
        <td>Allah Buksh</td>
        <td>BE-434</td>
        <td>Belt</td>
        <td>3</td>
        <td>Rs. 999</td>
      </tr>
      <tr>
        <td>Silver - 925</td>
        <td>N-912</td>
        <td>Necklace</td>
        <td>3</td>
        <td>Rs. 3,000</td>
      </tr>
      <tr>
        <td>Rolex</td>
        <td>W-121</td>
        <td>Watch</td>
        <td>3</td>
        <td>Rs. 180,000</td>
      </tr>
      <tr>
        <td>Tommy Hilfiger</td>
        <td>WT-112</td>
        <td>Wallet</td>
        <td>8</td>
        <td>Rs. 8000</td>
      </tr>
      <tr>
        <td>Urban Outfitters</td>
        <td>BR-667</td>
        <td>Bracelet</td>
        <td>18</td>
        <td>Rs. 1000</td>
      </tr>
      <tr>
        <td>Silver - 925</td>
        <td>N-912</td>
        <td>Necklace</td>
        <td>3</td>
        <td>Rs. 3,000</td>
      </tr>
      <tr>
        <td>Allah Buksh</td>
        <td>BE-434</td>
        <td>Belt</td>
        <td>3</td>
        <td>Rs. 999</td>
      </tr>
      <tr>
        <td>Silver - 925</td>
        <td>N-912</td>
        <td>Necklace</td>
        <td>3</td>
        <td>Rs. 3,000</td>
      </tr>
      <tr>
        <td>Rolex</td>
        <td>W-121</td>
        <td>Watch</td>
        <td>3</td>
        <td>Rs. 180,000</td>
      </tr>
      <tr>
        <td>Tommy Hilfiger</td>
        <td>WT-112</td>
        <td>Wallet</td>
        <td>8</td>
        <td>Rs. 8000</td>
      </tr>
      <tr>
        <td>Urban Outfitters</td>
        <td>BR-667</td>
        <td>Bracelet</td>
        <td>18</td>
        <td>Rs. 1000</td>
      </tr>
      <tr>
        <td>Silver - 925</td>
        <td>N-912</td>
        <td>Necklace</td>
        <td>3</td>
        <td>Rs. 3,000</td>
      </tr>
      <tr>
        <td>Allah Buksh</td>
        <td>BE-434</td>
        <td>Belt</td>
        <td>3</td>
        <td>Rs. 999</td>
      </tr>
      <tr>
        <td>Silver - 925</td>
        <td>N-912</td>
        <td>Necklace</td>
        <td>3</td>
        <td>Rs. 3,000</td>
      </tr> -->
    </table>
  </div>
</div>
<?php include('footer.php'); ?>