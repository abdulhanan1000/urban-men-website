<?php
    include("db.php");




    $sql = "SELECT * FROM inventory";
    $result = $conn->query($sql);

    $out = "<table border='0.5px' width='100%'>
            <tr>
                <th>Name</th>
                <th>ArticleNumber</th>
                <th>Category</th>
                <th>Quantity</th>
                <th>Price</th>
            </tr>
    
    ";

    if ($result->num_rows > 0) {
      while ($row = $result->fetch_assoc()) {
        $out .= "
        <tr>
      <td>" . $row['name'] . "</td>
      <td>" . $row['artno'] . "</td>
      <td>" . $row['category'] . "</td>
      <td>" . $row['qty'] . "</td>
      <td>" . $row['price'] . "</td>
        ";
      }
    }
    

    $out .= "</table>";

   

    header( "Content-Type: application/vnd.ms-excel" );
    header( "Content-disposition: attachment; filename=Inventory.xls" );
    echo $out;
?>